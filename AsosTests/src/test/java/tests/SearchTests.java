package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTests extends BaseTest {

    private String SEARCH_KEYWORD = "New Balance";
    private String EXPECTED_MAIN_PAGE_URL = "https://www.asos.com/";

    @Test
    public void checkThatUseLogoYouGoToMainPage(){
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().implicitWait(10);
        getSearchResultNewBalancePage().clickOnBigMainLogo();
        assertEquals(getDriver().getCurrentUrl(), EXPECTED_MAIN_PAGE_URL);
    }

    @Test
    public void checkThatSearchIsWorking(){
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().implicitWait(10);
        for( WebElement webElement : getSearchResultNewBalancePage().getElementNewBalanceListText()){
            assertTrue(webElement.getText().contains(SEARCH_KEYWORD));
        }
    }
}
