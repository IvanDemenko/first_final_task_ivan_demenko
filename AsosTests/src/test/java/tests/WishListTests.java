package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class WishListTests extends BaseTest {

    private String WISHLIST_ITEMS_COUNTER = "1 item";
    private String EMPTY_WISHLIST_TEXT = "You have no Saved Items";

    @Test
    public void checkAddingToWishList(){
        getHomePage().clickOnMenButton();
        getBasePage().waitForPageLoadComplete(30);
        getMenPage().clickOnCapsAssortmentButton();
        getBasePage().waitForPageLoadComplete(30);
        getCapsAssortmentPage().clickOnCapsLikeButton();
        getHomePage().clickOnViewWishListButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitForVisibilityOfElement(30, getWishListPage().getWishListText());
        assertEquals(getWishListPage().getWishListCounter(), WISHLIST_ITEMS_COUNTER);
    }

    @Test
    public void checkDeletingFromWishList(){
        getHomePage().clickOnMenButton();
        getBasePage().waitForPageLoadComplete(30);
        getMenPage().clickOnCapsAssortmentButton();
        getBasePage().waitForPageLoadComplete(30);
        getCapsAssortmentPage().clickOnCapsLikeButton();
        getHomePage().clickOnViewWishListButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitForVisibilityOfElement(30, getWishListPage().getWishListText());
        getWishListPage().clickOnDeleteItemFromWishListButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitForVisibilityOfElement(30, getWishListPage().getEmptyWishListText());
        assertEquals(getWishListPage().getEmptyWishList(), EMPTY_WISHLIST_TEXT);
    }
}
