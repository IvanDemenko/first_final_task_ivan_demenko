package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import pages.*;

public class BaseTest {

    private WebDriver driver;
    private static final String ASOS_URL = "https://www.asos.com/";

    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(ASOS_URL);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }


    public WebDriver getDriver() {
        return driver;
    }

    public BasePage getBasePage() {
        return new BasePage(getDriver());
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public MenPage getMenPage() {
        return new MenPage(getDriver());
    }

    public BootsAssortmentPage getBootsAssortmentPage() {
        return new BootsAssortmentPage(getDriver());
    }

    public BootsPage getBootsPage() {
        return new BootsPage(getDriver());
    }

    public CapsPage getCapsPage() {
        return new CapsPage(getDriver());
    }

    public CapsAssortmentPage getCapsAssortmentPage() {
        return new CapsAssortmentPage(getDriver());
    }

    public WishListPage getWishListPage() {
        return new WishListPage(getDriver());
    }

    public SearchResultNewBalancePage getSearchResultNewBalancePage() {
        return new SearchResultNewBalancePage(getDriver());
    }

    public JacketsAndCoatsPage getJacketsAndCoatsPage() {
        return new JacketsAndCoatsPage(getDriver());
    }

    public AuthorizationPage getAuthorizationPage() {
        return new AuthorizationPage(getDriver());
    }

}
