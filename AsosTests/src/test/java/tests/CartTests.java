package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CartTests extends BaseTest {

    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_AFTER_ADDING = "1";
    private String EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_AFTER_DELETING = "0";

    @Test
    public void checkAddToCart(){
        getHomePage().clickOnMenButton();
        getBasePage().waitForPageLoadComplete(30);
        getMenPage().clickOnBootsAssortmentButton();
        getBasePage().waitForPageLoadComplete(30);
        getBootsAssortmentPage().clickOnBootsButton();
        getBasePage().waitForPageLoadComplete(30);
        getBootsPage().clickOnBootsSizeButton();
        getBootsPage().clickOnBootsSize42Button();
        getBootsPage().clickOnBootsAddToCartButton();
        getBasePage().waitForVisibilityOfElement(30, getHomePage().getMiniBagDropdownPopup());
        assertEquals(getHomePage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_AFTER_ADDING);
    }

    @Test
    public void checkDeleteFromCart(){
        getHomePage().clickOnMenButton();
        getBasePage().waitForPageLoadComplete(30);
        getMenPage().clickOnBootsAssortmentButton();
        getBasePage().waitForPageLoadComplete(30);
        getBootsAssortmentPage().clickOnBootsButton();
        getBasePage().waitForPageLoadComplete(30);
        getBootsPage().clickOnBootsSizeButton();
        getBootsPage().clickOnBootsSize42Button();
        getBootsPage().clickOnBootsAddToCartButton();
        getBasePage().waitForVisibilityOfElement(30, getHomePage().getMiniBagDropdownPopup());
        getBasePage().moveToElement(getHomePage().getMiniBagDropdownPopup());
        getHomePage().clickOnViewBagButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().implicitWait(10);
        getCapsPage().clickOnRemoveItemFromCartButton();
        getBasePage().waitForVisibilityOfElement(30, getCapsPage().getCartText());
        assertEquals(getHomePage().getTextOfAmountOfProductsInCartAfterDeleting(), EXPECTED_AMOUNT_OF_PRODUCTS_IN_CART_AFTER_DELETING);
    }
}
