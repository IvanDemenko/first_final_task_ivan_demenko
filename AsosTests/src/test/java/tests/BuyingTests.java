package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class BuyingTests extends BaseTest {

    private String SELECT_SIZE_ERROR_MESSAGE = "Please select from the available colour and size options";

    @Test
    public void checkBuyingWithWrongSize(){
        getHomePage().clickOnMenButton();
        getBasePage().waitForPageLoadComplete(30);
        getMenPage().clickOnBootsAssortmentButton();
        getBasePage().waitForPageLoadComplete(30);
        getBootsAssortmentPage().clickOnBootsButton();
        getBasePage().waitForPageLoadComplete(30);
        getBootsPage().clickOnBootsAddToCartButton();
        assertEquals(getBootsPage().getSelectSizeError(), SELECT_SIZE_ERROR_MESSAGE);
    }
}
