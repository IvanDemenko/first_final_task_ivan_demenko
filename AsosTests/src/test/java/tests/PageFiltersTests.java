package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class PageFiltersTests extends BaseTest {

    private String SEARCH_KEYWORD_FILTER_BY_BRAND_ELLESSE = "ellesse";

    @Test
    public void checkThatFilterByBrandIsWorking(){
        getHomePage().clickOnMenButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().moveToElement(getMenPage().getTypeOfProductsNav());
        getBasePage().implicitWait(10);
        getMenPage().clickOnJacketsAndCoatsButton();
        getBasePage().waitForPageLoadComplete(30);
        getJacketsAndCoatsPage().clickOnFilterByBrandButton();
        getBasePage().implicitWait(10);
        getJacketsAndCoatsPage().clickOnFilterByEllesseButton();
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().waitForVisibilityOfElement(30, getJacketsAndCoatsPage().getFilterText());
        getBasePage().implicitWait(10);
        for( WebElement webElement : getJacketsAndCoatsPage().getElementEllesseListText()){
            assertTrue(webElement.getText().contains(SEARCH_KEYWORD_FILTER_BY_BRAND_ELLESSE));
        }
    }
}
