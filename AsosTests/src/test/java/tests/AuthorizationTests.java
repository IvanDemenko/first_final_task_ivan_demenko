package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AuthorizationTests extends BaseTest {

    private String EMAIL_ADDRESS = "test1234test1234@test.com";
    private String ERRORE_MASSAGE = "Hey, we need a password here";

    @Test
    public void checkAuthorizationWithOutPassword(){
        getBasePage().moveToElement(getHomePage().getAuthorizationButton());
        getBasePage().implicitWait(10);
        getHomePage().clickOnMyAccountButton();
        getBasePage().waitForPageLoadComplete(30);
        getAuthorizationPage().emailAddress(EMAIL_ADDRESS);
        getAuthorizationPage().submitBySignInButton();
        getBasePage().implicitWait(10);
        assertEquals(getAuthorizationPage().getSignInErrorMessage(), ERRORE_MASSAGE);
    }
}
