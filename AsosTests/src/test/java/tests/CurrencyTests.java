package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CurrencyTests extends BaseTest {

    private String SEARCH_KEYWORD = "New Balance";
    private String SEARCH_KEYWORD1 = "$";

    @Test
    public void checkThatCurrencyChangeIsPossible(){
        getHomePage().clickOnCountrySelectorButton();
        getBasePage().waitForVisibilityOfElement(30, getHomePage().getCountrySelectorPopup());
        getHomePage().clickOnTypeOfCurrencyButton();
        getBasePage().implicitWait(10);
        getHomePage().clickOnTakeUsdCurrencyButton();
        getHomePage().clickOnUpdateButton();
        getBasePage().waitForPageLoadComplete(30);
        getHomePage().searchByKeyword(SEARCH_KEYWORD);
        getBasePage().waitForPageLoadComplete(30);
        getBasePage().implicitWait(10);
        for( WebElement webElement : getSearchResultNewBalancePage().getElementNewBalanceListPrice()){
            assertTrue(webElement.getText().contains(SEARCH_KEYWORD1));
        }
    }
}
