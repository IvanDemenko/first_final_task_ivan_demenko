package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MenPage extends BasePage {

    @FindBy(xpath = "//a[@class='feature__link'][contains(@href,'boots')]")
    private WebElement bootsAssortmentButton;

    @FindBy(xpath = "//a[@class='feature__link'][contains(@href,'caps')]")
    private WebElement capsAssortmentButton;

    @FindBy(xpath = "//nav[@aria-hidden='false']//button[contains(@class,'_2syfS2P')][@data-index='1']")
    private WebElement typeOfProductsNav;

    @FindBy(xpath = "//a[text()='Jackets & Coats']")
    private WebElement jacketsAndCoatsButton;

    public MenPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnBootsAssortmentButton() {
        bootsAssortmentButton.click();
    }

    public void clickOnCapsAssortmentButton() {
        capsAssortmentButton.click();
    }

    public WebElement getTypeOfProductsNav() {
        return typeOfProductsNav;
    }

    public void clickOnJacketsAndCoatsButton() {
        jacketsAndCoatsButton.click();
    }
}
