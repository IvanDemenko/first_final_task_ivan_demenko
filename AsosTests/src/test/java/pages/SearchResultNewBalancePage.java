package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultNewBalancePage extends BasePage {

    @FindBy(xpath = "//a[@data-testid='asoslogo']")
    private WebElement bigMainLogo;

    @FindBy(xpath = "//div[@class='_3J74XsK']//p")
    private List<WebElement> searchResultsProductsListText;

    @FindBy(xpath = "//span[@class='_16nzq18']")
    private List<WebElement> searchResultsProductsListPrice;

    public SearchResultNewBalancePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnBigMainLogo() {
        bigMainLogo.click();
    }

    public List<WebElement> getElementNewBalanceListText() {
        return searchResultsProductsListText;
    }

    public List<WebElement> getElementNewBalanceListPrice() {
        return searchResultsProductsListPrice;
    }
}
