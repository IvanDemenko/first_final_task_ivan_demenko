package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "//a[contains(@class,'TO7hyVB')][@data-testid='men-floor']")
    private WebElement menButton;

    @FindBy(xpath = "//div[@id='miniBagDropdown']//div[@class='_33s2s-y']")
    private WebElement miniBagDropdownPopup;

    @FindBy(xpath = "//span[@class='_1z5n7CN']")
    private WebElement amountOfProductsInCart;

    @FindBy(xpath = "//span[text()='View Bag']")
    private WebElement viewBagButton;

    @FindBy(xpath = "//a[@type='a'][contains(@href,'bag')]")
    private WebElement amountOfProductsInCartAfterDeleting;

    @FindBy(xpath = "//a[@aria-label='Saved Items']")
    private WebElement viewWishListButton;

    @FindBy(xpath = "//input[@type='search']")
    private WebElement searchInput;

    @FindBy(xpath = "//div[@data-testid='topbar']//button[@data-testid='country-selector-btn']")
    private WebElement countrySelectorButton;

    @FindBy(xpath = "//section[@role='dialog']")
    private WebElement countrySelectorPopup;

    @FindBy(xpath = "//select[@id='currency']")
    private WebElement typeOfCurrencyButton;

    @FindBy(xpath = "//option[contains(text(),'USD')]")
    private WebElement takeUsdCurrencyButton;

    @FindBy(xpath = "//button[contains(text(),'Update')]")
    private WebElement updateButton;

    @FindBy(xpath = "//button[contains(@class,'_6iPIuvw ')]")
    private WebElement authorizationButton;

    @FindBy(xpath = "//a[text()='My Account']")
    private WebElement myAccountButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnMenButton() {
        menButton.click();
    }

    public WebElement getMiniBagDropdownPopup() {
        return miniBagDropdownPopup;
    }

    public String getTextOfAmountProductsInCart() {
        return amountOfProductsInCart.getText();
    }

    public String getTextOfAmountOfProductsInCartAfterDeleting() {
        return amountOfProductsInCartAfterDeleting.getText();
    }

    public void clickOnViewBagButton() {
        viewBagButton.click();
    }

    public void clickOnViewWishListButton() {
        viewWishListButton.click();
    }

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickOnCountrySelectorButton() {
        countrySelectorButton.click();
    }

    public WebElement getCountrySelectorPopup() {
        return countrySelectorPopup;
    }

    public void clickOnTypeOfCurrencyButton() {
        typeOfCurrencyButton.click();
    }

    public void clickOnTakeUsdCurrencyButton() {
        takeUsdCurrencyButton.click();
    }

    public void clickOnUpdateButton() {
        updateButton.click();
    }

    public WebElement getAuthorizationButton() {
        return authorizationButton;
    }

    public void clickOnMyAccountButton() {
        myAccountButton.click();
    }
}
