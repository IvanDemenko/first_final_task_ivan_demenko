package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BootsAssortmentPage extends BasePage {

    @FindBy(xpath = "//a[@class='_3TqU78D'][contains(@aria-label,'pull tabs in black')]")
    private WebElement bootsButton;

    public BootsAssortmentPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnBootsButton() {
        bootsButton.click();
    }
}
