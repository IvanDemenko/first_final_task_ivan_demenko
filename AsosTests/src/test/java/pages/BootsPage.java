package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BootsPage extends BasePage{

    @FindBy(xpath = "//select[contains(@id,'main-size')]")
    private WebElement bootsSizeButton;

    @FindBy(xpath = "//option[text()='EU 42']")
    private WebElement bootsSize42Button;

    @FindBy(xpath = "//a[@class='add-button']")
    private WebElement bootsAddToCartButton;

    @FindBy(xpath = "//span[@id='selectSizeError']")
    private WebElement selectSizeError;

    public BootsPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnBootsSizeButton() {
        bootsSizeButton.click();
    }

    public void clickOnBootsSize42Button() {
        bootsSize42Button.click();
    }

    public void clickOnBootsAddToCartButton() {
        bootsAddToCartButton.click();
    }

    public String getSelectSizeError() {
        return selectSizeError.getText();
    }

}
