package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class JacketsAndCoatsPage extends BasePage {

    @FindBy(xpath = "//li[@data-auto-id='brand']//button[@class='_1om7l06']")
    private WebElement filterByBrandButton;

    @FindBy(xpath = "//label[contains(text(),'ellesse')]")
    private WebElement filterByEllesseButton;

    @FindBy(xpath = "//p[text()='1 selected']")
    private WebElement filterText;

    @FindBy(xpath = "//div[@class='_3J74XsK']//p")
    private List<WebElement> searchResultsByFilterProductsListText;

    public JacketsAndCoatsPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnFilterByBrandButton() {
        filterByBrandButton.click();
    }

    public void clickOnFilterByEllesseButton() {
        filterByEllesseButton.click();
    }

    public WebElement getFilterText() {
        return filterText;
    }

    public List<WebElement> getElementEllesseListText() {
        return searchResultsByFilterProductsListText;
    }
}
