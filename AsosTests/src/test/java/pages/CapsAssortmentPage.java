package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CapsAssortmentPage extends BasePage {

    @FindBy(xpath = "//a[contains(@aria-label,'cap in black with serif logo')]/following-sibling::button[@data-auto-id='saveForLater']")
    private WebElement capsLikeButton;

    public CapsAssortmentPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnCapsLikeButton() {
        capsLikeButton.click();
    }
}
