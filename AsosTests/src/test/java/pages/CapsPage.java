package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CapsPage extends BasePage {

    @FindBy(xpath = "//button[@class='bag-item-remove']")
    private WebElement removeItemFromCartButton;

    @FindBy(xpath = "//h2[text()='Your bag is empty']")
    private WebElement cartText;

    public CapsPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnRemoveItemFromCartButton() {
        removeItemFromCartButton.click();
    }

    public WebElement getCartText() {
        return cartText;
    }
}
